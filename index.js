// Import thư viên express.js vào. Dạng Import express from "express"
const express = require("express");

// Khởi tạo app express
const app = express();

// Khai báo sẵn 1 port trên hệ thống
const port = 8000;

// Khai báo API
app.get("/", (req, res) => {
    let today = new Date();

    // Response trả về 1 chuỗi JSON có status 200
    res.status(200).json({
        message: `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`
    })
})

// Callback function
// + Nó là 1 function
// + Nó là 1 tham số của 1 hàm khác
// + Nó sẽ chạy sau khi hàm chủ thể được thực hiện
// Lắng nghe app trên cổng được khai báo 8000
app.listen(port, () => {
    console.log("App listening on port: ", port);
});